package hello.config;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudsearchdomain.AmazonCloudSearchDomainClient;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * Created by andre on 27/07/2016.
 */

@Configuration
public class ApplicationConfig implements EnvironmentAware {

    @Resource
    Environment env;

    private RelaxedPropertyResolver springPropertyResolver;

    @Override
    public void setEnvironment(Environment env) {
        this.env = env;
        this.springPropertyResolver = new RelaxedPropertyResolver(env, "spring.");
    }

    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(env.getProperty("aws.accessKey"),
                env.getProperty("aws.secretKey"));
    }

    @Bean
    public AmazonCloudSearchDomainClient amazonCloudSearchDomainClient() {
        AmazonCloudSearchDomainClient cloudSearch = new AmazonCloudSearchDomainClient(basicAWSCredentials());
        cloudSearch.setEndpoint(env.getProperty("aws.cloudsearchEndpoint"));
        return cloudSearch;
    }
}
